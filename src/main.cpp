#include <SFML/Graphics.hpp>
#include <iostream>
#include <SFML/System.hpp>
#include <random>

using namespace sf;

#define TILE_SPRITE_PATH "res/blocks.png"
#define FIELD_WIDTH 13
#define FIELD_HEIGHT 25
#define FIELD_CELL_SIZE 32
#define SHAPE_SIZE 4
#define SPRITES_PER_SHAPE SHAPE_SIZE * SHAPE_SIZE
#define AMOUNT_SHAPES 7

int ticksPerUpdate = 30;
int tickCount = 0;

Sprite *field[FIELD_WIDTH * FIELD_HEIGHT];

int *i_shape[4] = {
    new int[SPRITES_PER_SHAPE] {
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        1, 1, 1, 1,
        0, 0, 0, 0,
        0, 0, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 1, 0,
        0, 0, 1, 0,
        0, 0, 1, 0,
        0, 0, 1, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 0, 0,
        1, 1, 1, 1,
        0, 0, 0, 0
    }
};

int *o_shape[4] = {
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 1, 1, 0,
        0, 1, 1, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 1, 1, 0,
        0, 1, 1, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 1, 1, 0,
        0, 1, 1, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 1, 1, 0,
        0, 1, 1, 0
    }
};

int *t_shape[4] = {
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 0, 0,
        1, 1, 1, 0,
        0, 1, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 1, 0, 0,
        1, 1, 0, 0,
        0, 1, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 1, 0, 0,
        1, 1, 1, 0,
        0, 0, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 1, 0, 0,
        0, 1, 1, 0,
        0, 1, 0, 0
    }
};

int *l_shape[4] = {
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 1, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 0, 0,
        1, 1, 1, 0,
        1, 0, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        1, 1, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 1, 0,
        1, 1, 1, 0,
        0, 0, 0, 0
    }
};

int *j_shape[4] = {
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 1, 0, 0,
        0, 1, 0, 0,
        1, 1, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        1, 0, 0, 0,
        1, 1, 1, 0,
        0, 0, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 1, 1, 0,
        0, 1, 0, 0,
        0, 1, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 0, 0,
        1, 1, 1, 0,
        0, 0, 1, 0
    }
};

int *z_shape[4] = {
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 0, 0,
        1, 1, 0, 0,
        0, 1, 1, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 1, 0, 0,
        1, 1, 0, 0,
        1, 0, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        1, 1, 0, 0,
        0, 1, 1, 0,
        0, 0, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 1, 0,
        0, 1, 1, 0,
        0, 1, 0, 0
    }
};

int *s_shape[4] = {
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 1, 1, 0,
        1, 1, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        1, 0, 0, 0,
        1, 1, 0, 0,
        0, 1, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 1, 1, 0,
        1, 1, 0, 0,
        0, 0, 0, 0
    },
    new int[SPRITES_PER_SHAPE] {
        0, 0, 0, 0,
        0, 1, 0, 0,
        0, 1, 1, 0,
        0, 0, 1, 0
    }
};

int **pShapes[] = {
    i_shape,
    t_shape,
    o_shape,
    z_shape,
    s_shape,
    j_shape,
    l_shape
};

int lastShapeIndex = -1;

int **currentShape;
Texture *currentTex;
Vector2f shapePositionInField;
Sprite *pShapeControlled[SPRITES_PER_SHAPE];
int rotationIndex = 0;
bool canRotate = true;

const IntRect rect_blue_tile = IntRect(0, 0, 32, 32);
const IntRect rect_green_tile = IntRect(32, 0, 32, 32);
const IntRect rect_grey_tile = IntRect(64, 0, 32, 32);
const IntRect rect_purple_tile = IntRect(96, 0, 32, 32);
const IntRect rect_red_tile = IntRect(128, 0, 32, 32);

Texture texBlue, texGreen, texGrey, texPurple, texRed;

Texture *tile_textures[] = {
    &texBlue,
    &texGreen,
    &texGrey,
    &texPurple,
    &texRed
};

bool hShapeMoved = false;
bool shapeCollided = false;
bool spawnNewShape = true;

void spawnRandomShape();
void rotateShape(int direction);
bool shapeCollidesWithFieldTiles();


int pickRandomTileNumber()
{
    int randomIndex = rand() % 5;
    return randomIndex;
}

void releaseControlledShape()
{
    for (int i = 0; i < SPRITES_PER_SHAPE; i++)
    {
        if (pShapeControlled[i] != NULL)
        {
            int x = pShapeControlled[i]->getPosition().x / FIELD_CELL_SIZE;
            int y = pShapeControlled[i]->getPosition().y / FIELD_CELL_SIZE;

            field[x + (y * FIELD_WIDTH)] = pShapeControlled[i];
        }
    }

    shapePositionInField.x = 0;
    shapePositionInField.y = 0;
}

void clearControlledShape()
{
    for (int i = 0; i < SPRITES_PER_SHAPE; i++)
    {
        if (pShapeControlled[i] != NULL)
        {

            pShapeControlled[i] = NULL;
        }
    }
}

bool shapeCollidesWithFieldTiles()
{
    for (int i = 0; i < SPRITES_PER_SHAPE; i++)
    {
        if (pShapeControlled[i] != NULL) {
            int localX = i % SHAPE_SIZE;
            int localY = i / SHAPE_SIZE;

            int x = pShapeControlled[i]->getPosition().x / FIELD_CELL_SIZE; 
            int y = pShapeControlled[i]->getPosition().y / FIELD_CELL_SIZE;

            if (field[x + (y * FIELD_WIDTH)] != NULL)
            {
                return true;
            }
        }
    }

    return false;
}

void checkFilledRows()
{
    for (int row = 0; row < FIELD_HEIGHT; row++)
    {
        bool rowFilled = true;
        for (int i = 0; i < FIELD_WIDTH; i++)
        {
            if (field[row * FIELD_WIDTH + i] == NULL)
            {
                rowFilled = false;
                break;
            }
        }

        if (rowFilled)
        {
            // Clear filled rows
            for (int i = 0; i < FIELD_WIDTH; i++)
            {
                delete field[row * FIELD_WIDTH + i];
                field[row * FIELD_WIDTH + i] = NULL;
            }
            clearControlledShape();
        }
    }

    for (int row = 0; row < FIELD_HEIGHT; row++)
    {
        bool rowEmpty = false;
        for (int i = 0; i < FIELD_WIDTH; i++)
        {
            Sprite *currentTile = field[row*FIELD_WIDTH + i];
            if (currentTile != NULL)
            {
                rowEmpty = false;
                break;
            }

            rowEmpty = true;
        }

        if (rowEmpty) {
            // This row is empty, shift all rows above
            for (int r = row; r > 0; r--)
            {
                for (int j = 0; j < FIELD_WIDTH; j++)
                {
                    Sprite *tileAbove = field[((r-1) * FIELD_WIDTH) + j];
                    field[((r-1) * FIELD_WIDTH) + j] = NULL;
                    
                    field[(r * FIELD_WIDTH) + j] = tileAbove;

                    if (tileAbove != NULL)
                    {
                        tileAbove->move(0, FIELD_CELL_SIZE);
                    }
                }
            }
        }
    }
}

void moveShape(int dx, int dy)
{
    for (int i = 0; i < SPRITES_PER_SHAPE; i++)
    {
        if (pShapeControlled[i] != NULL)
        {
            Vector2f tileSpritePos = pShapeControlled[i]->getPosition();

            if (dx < 0 && (tileSpritePos.x + dx) / FIELD_CELL_SIZE < 0 || 
                dx > 0 && (tileSpritePos.x + dx) / FIELD_CELL_SIZE > FIELD_WIDTH - 1)
            {
                return;
            }

            if (dy > 0 && (tileSpritePos.y + dy) / FIELD_CELL_SIZE > FIELD_HEIGHT-1)
            {
                shapeCollided = true;
                return;
            }
        }
    }

    shapePositionInField.x += dx;
    shapePositionInField.y += dy;

    for (int i = 0; i < SPRITES_PER_SHAPE; i++)
    {
        if (pShapeControlled[i] != NULL)
        {
            int localX = i % SHAPE_SIZE;
            int localY = i / SHAPE_SIZE;

            int spriteX = (localX + shapePositionInField.x) * FIELD_CELL_SIZE;
            int spriteY = (localY + shapePositionInField.y) * FIELD_CELL_SIZE;
            
            pShapeControlled[i]->setPosition(spriteX, spriteY);
        }
    }

    if (shapeCollidesWithFieldTiles())
    {
        moveShape(-dx, -dy);
        
        if (dy > 0)
        {
            // Shape fell on tiles
            shapeCollided = true;
        }
    }
}

void update()
{
    if (Keyboard::isKeyPressed(Keyboard::Left) && !hShapeMoved)
    {
        hShapeMoved = true;
        moveShape(-1, 0);
    } else if (Keyboard::isKeyPressed(Keyboard::Right) && !hShapeMoved)
    {
        hShapeMoved = true;
        moveShape(1, 0);
    }

    if (!Keyboard::isKeyPressed(Keyboard::Right) && !Keyboard::isKeyPressed(Keyboard::Left))
    {
        hShapeMoved = false;
    }

    if (Keyboard::isKeyPressed(Keyboard::Down))
    {
        ticksPerUpdate = 5;
    } else {
        ticksPerUpdate = 30;
    }

    if (Keyboard::isKeyPressed(Keyboard::Up))
    {
        if (canRotate) {
            canRotate = false;
            rotateShape(1);
        }
    } else {
        canRotate = true;
    }

    if (spawnNewShape)
    {
        spawnRandomShape();
        spawnNewShape = false;
    }

    if (shapeCollided)
    {
        shapeCollided = false;
        spawnNewShape = true;
        releaseControlledShape();
        checkFilledRows();
    }


    tickCount++;
    if (tickCount >= ticksPerUpdate) {
        moveShape(0, 1);
        tickCount = 0;
    }
}

void spawnRandomShape()
{
    int randomNum = 0;
    while (randomNum == lastShapeIndex)
    {
        randomNum = rand() % AMOUNT_SHAPES;
    }
    lastShapeIndex = randomNum;

    currentShape = pShapes[randomNum];
    currentTex = tile_textures[pickRandomTileNumber()];

    shapePositionInField.x = 0;
    shapePositionInField.y = 0;

    for (int i = 0; i < SPRITES_PER_SHAPE; i++)
    {
        pShapeControlled[i] = NULL;

        int localX = i%SHAPE_SIZE;
        int localY = i/SHAPE_SIZE;
        if (currentShape[rotationIndex][i] == 1)
        {
            pShapeControlled[i] = new Sprite(*currentTex);
            pShapeControlled[i]->setPosition(localX * FIELD_CELL_SIZE, localY * FIELD_CELL_SIZE);
        }
    }
}

void rotateShape(int direction)
{
    if (direction > 0) {
        rotationIndex += 1;
    } else if (direction < 0) {
        rotationIndex -= 1;
    }

    if (rotationIndex < 0) {
        rotationIndex = 3;
    } else if (rotationIndex > 3) {
        rotationIndex = 0;
    }

    for (int i = 0; i < SPRITES_PER_SHAPE; i++)
    {
        delete pShapeControlled[i];
        pShapeControlled[i] = NULL;

        int localX = i % SHAPE_SIZE;
        int localY = i / SHAPE_SIZE;

        if (currentShape[rotationIndex][i] == 1)
        {
            pShapeControlled[i] = new Sprite(*currentTex);
            pShapeControlled[i]->setPosition(localX * FIELD_CELL_SIZE + shapePositionInField.x * FIELD_CELL_SIZE, localY * FIELD_CELL_SIZE + shapePositionInField.y * FIELD_CELL_SIZE);
        }
    }

    if (shapeCollidesWithFieldTiles())
    {
        rotateShape(-1);
    }
}

int main()
{
    RenderWindow window(sf::VideoMode(416, 800), "Let's make tetris");

    window.setFramerateLimit(60);

    texBlue.loadFromFile(TILE_SPRITE_PATH, rect_blue_tile);
    texGreen.loadFromFile(TILE_SPRITE_PATH, rect_green_tile);
    texGrey.loadFromFile(TILE_SPRITE_PATH, rect_grey_tile);
    texPurple.loadFromFile(TILE_SPRITE_PATH, rect_purple_tile);
    texRed.loadFromFile(TILE_SPRITE_PATH, rect_red_tile);
    
    while (window.isOpen())
    {
        Event event;
        while (window.pollEvent(event))
        {
            if (event.type == Event::Closed)
                window.close();
        }

        update();

        window.clear();

        // Draw controlled shape
        for (int i = 0; i < SPRITES_PER_SHAPE; i++)
        {
            if (pShapeControlled[i] != NULL)
            {
                window.draw(*pShapeControlled[i]);
            }
        }

        // Draw  field skip drawing the first 4 rows
        for (int i = 0/*FIELD_WIDTH * 4*/; i < FIELD_WIDTH * FIELD_HEIGHT; i++)
        {
            int x = i%FIELD_WIDTH;
            int y = i/FIELD_WIDTH;

            if (field[i] != NULL)
            {
                window.draw(*field[i]);
            }
        }
        

        window.display();
    }

    return 0;
}